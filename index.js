var CROSS = 'X';
var ZERO = 'O';
var EMPTY = ' ';
var numClick = 0;
var FIELD_SIZE = 0;
var moveMatrix;
var haveWin = false;

startGame();

function startGame() {
  FIELD_SIZE = +prompt('Введите размер поля:');
  moveMatrix = new Array(FIELD_SIZE);
  renderGrid(FIELD_SIZE);
}

/* Искуственный интеллект */

function skynetPlay(symbol) {
  let row = Math.ceil(Math.random() * FIELD_SIZE - 1);
  let col = Math.ceil(Math.random() * FIELD_SIZE - 1);
  console.log(`row: ${row}; col ${col}; `);
  while (moveMatrix[row][col] !== EMPTY) {
    row = Math.ceil(Math.random() * FIELD_SIZE - 1);
    col = Math.ceil(Math.random() * FIELD_SIZE - 1);
  }
  moveMatrix[row][col] = symbol;
  renderSymbolInCell(symbol, row, col);
  checkWhoWin(CROSS, row, col);
  numClick++;
}

/* обработчик нажатия на клетку */
function cellClickHandler (row, col) {
  // Пиши код тут
  console.log(`Clicked on cell: ${row}, ${col}`);

  if (haveWin) { 
    return;
  }

  if (numClick % 2) {
    if (moveMatrix[row][col] !== EMPTY) {
      return;
    }
    renderSymbolInCell(CROSS, row, col);
    moveMatrix[row][col] = CROSS;
    checkWhoWin(CROSS, row, col);
  }
  else { 
    if (moveMatrix[row][col] !== EMPTY) {
      return;
    }
    renderSymbolInCell(ZERO, row, col);
    moveMatrix[row][col] = ZERO;
    checkWhoWin(ZERO, row, col);
  }

  numClick++;

  if (!haveWin) {
    skynetPlay(CROSS);
  }

  if (numClick === FIELD_SIZE*FIELD_SIZE) {
    showMessage('Победила дружба');
  }
  /* Пользоваться методом для размещения символа в клетке так:
      renderSymbolInCell(ZERO, row, col);
   */
}

function checkWhoWin(symbol, row, col) {
  let numEqSymbols = 0;
  /* Проверяем строку */
  for (let i = 0; i < FIELD_SIZE; i++) { 
    let cell = moveMatrix[row][i];
    if (cell === symbol) {
      numEqSymbols++;
    }
  }
  console.log(typeof (numEqSymbols));
  console.log(typeof (FIELD_SIZE));
  if (numEqSymbols === FIELD_SIZE) { 
    console.log('Here');
    showMessage(`Wimming is ${symbol}`);
    endOfGame('r', row, col);
    return;
  }
  
/* Проверяем столбец */
  numEqSymbols = 0
  for (let i = 0; i < FIELD_SIZE; i++) { 
    let cell = moveMatrix[i][col];
    
    if (cell === symbol) {
      numEqSymbols++;
    }
  }
  console.log(numEqSymbols);
  if (numEqSymbols === FIELD_SIZE) { 
    showMessage(`Wimming is ${symbol}`);
    endOfGame('c', row, col);
    return;
  }

/* Проверяем диагональ */
  let numEqSymbolsGD = 0;
  let numEqSymbolsPD = 0;
  for (let i = 0; i < FIELD_SIZE; i++) { 
    /* Главная диагональ */
    if (moveMatrix[i][i] === symbol) {
      numEqSymbolsGD++;
    }
    /* Побочная диагональ */
    if (moveMatrix[i][FIELD_SIZE - i-1] === symbol) {
      numEqSymbolsPD++;
    }
  }
  console.log(`Главная диагональ: ${numEqSymbolsGD}`);
  console.log(`Побочная диагональ: ${numEqSymbolsPD}`);

  if (numEqSymbolsGD === FIELD_SIZE) {
    showMessage(`Wimming is ${symbol}`);
    endOfGame('g', row, col);
    return;
  }
  if (numEqSymbolsPD === FIELD_SIZE) {
    showMessage(`Wimming is ${symbol}`);
    endOfGame('p', row, col);
  }
}


/* У нас победитель */
function endOfGame(where, row, col) { 
  switch (where) {
    case 'r': /*row*/
      console.log('row');
      for (let i = 0; i < FIELD_SIZE; i++) {
        let cell = findCell(row, i);
        cell.style.color = 'red';
      }
      break;
    case 'c': /* column */
      console.log('column');
      for (let i = 0; i < FIELD_SIZE; i++) {
        let cell = findCell(i, col);
        cell.style.color = 'red';
      }
      break;
    case 'g': /* Главная диагональ */
      console.log('Главная диагональ');
      for (let i = 0; i < FIELD_SIZE; i++) {
        let cell = findCell(i, i);
        cell.style.color = 'red';
      }
      break;
    case 'p': /* Побочная диагональ */
      console.log('Побочная диагональ');
      for (let i = 0; i < FIELD_SIZE; i++) {
        let cell = findCell(i, FIELD_SIZE-i-1);
        cell.style.color = 'red';
      }
      break;
    default:
      console.log('Ooops,  problem in enfOfGame');
  }
  haveWin = true;
  console.log('end of game');
}

/* обработчик нажатия на кнопку "Сначала" */
function resetClickHandler () {
  console.log('reset!');
  for (let i = 0; i < FIELD_SIZE; i++) {
    for (let j = 0; j < FIELD_SIZE; j++) {
      moveMatrix[i][j] = EMPTY;
      renderSymbolInCell(EMPTY, i, j);
    }
  }
  haveWin = false;
  numClick = 0;
  showMessage(' ');
}

/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function showMessage(text) {
  var msg = document.querySelector('.message');
  msg.innerText = text
}

/* Нарисовать игровое поле заданного размера */
function renderGrid (dimension) {
  var container = getContainer();
  container.innerHTML = '';

  for (let i = 0; i < dimension; i++) {
    var row = document.createElement('tr');
    for (let j = 0; j < dimension; j++) {
      var cell = document.createElement('td');
      cell.textContent = EMPTY;
      cell.addEventListener('click', () => cellClickHandler(i, j));
      row.appendChild(cell);
    }
    container.appendChild(row);
  }
  /* */
  for (let i = 0; i < FIELD_SIZE; i++) { 
    moveMatrix[i] = new Array(FIELD_SIZE);
    for (let j = 0; j < FIELD_SIZE; j++) { 
      moveMatrix[i][j] = EMPTY;
    }
  }
  /********/
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell (symbol, row, col, color = '#333') {
  var targetCell = findCell(row, col);

  targetCell.textContent = symbol;
  targetCell.style.color = color;
}

function findCell (row, col) {
  var container = getContainer();
  var targetRow = container.querySelectorAll('tr')[row];
  return targetRow.querySelectorAll('td')[col];
}

function getContainer() {
  return document.getElementById('fieldWrapper');
}

/* Test Function */
/* Победа первого игрока */
function testWin () {
  clickOnCell(0, 2);
  clickOnCell(0, 0);
  clickOnCell(2, 0);
  clickOnCell(1, 1);
  clickOnCell(2, 2);
  clickOnCell(1, 2);
  clickOnCell(2, 1);
}

/* Ничья */
function testDraw () {
  clickOnCell(2, 0);
  clickOnCell(1, 0);
  clickOnCell(1, 1);
  clickOnCell(0, 0);
  clickOnCell(1, 2);
  clickOnCell(1, 2);
  clickOnCell(0, 2);
  clickOnCell(0, 1);
  clickOnCell(2, 1);
  clickOnCell(2, 2);
}

function clickOnCell (row, col) {
  findCell(row, col).click();
}
